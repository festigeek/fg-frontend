---
title: Numerik Games
subtitle: Festival numérique
category: Festival
layout: default
modal-id: 2
creation-date: Août 2016
img: ng2019.png
thumbnail: ng2019-thumbnail.png
alt: Numerik Games
size: 10'500 personnes
description: "Numerik Games Festival est une manifestation tout public dédiée à l’art et la culture numériques qui se tient tous les ans, fin août, à Y-Parc (Yverdon-les-Bains).
    <br />Résolu à vous en mettre plein les yeux, Numerik Games Festival aspire à valoriser le numérique à travers les différents champs qu’il peut couvrir: musical, culturel, artistique, patrimonial, économique et académique."
---